import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers/index';
import thunk from 'redux-thunk';

export default function configureStore(initialState) {
    const middleware = applyMiddleware(thunk)(createStore);
    return middleware(rootReducer, initialState, window.devToolsExtension && window.devToolsExtension());

}
