import React from 'react';
import reactCSS from 'reactcss';

const styles = reactCSS({
    'default': {
        ImageStyle: {
            marginTop: '-5px',
        },
        contentStyle: {
            marginBottom: '-5px'
        }
    },
});

const AvatarHeader = (props) => {
    var {username, avatarPath} = props;
    return (
        <div className="content" style={styles.ContentStyle}>
            <img className="ui avatar image" src={avatarPath} style={styles.imageStyle}/> {username}
        </div>
    )
};

AvatarHeader.defaultProps = {
    username: "username",
    avatarPath: "http://www.pdhbook.com/Images/User/default_image/User_Placeholder.png"
};
AvatarHeader.propTypes = {
    username: React.PropTypes.string,
    avatarPath: React.PropTypes.string,
};

export default AvatarHeader;
