import React from 'react';
import {connect} from 'react-redux';
import {toggleLike} from '../../../actions/postActions';

class LikeButton extends React.Component{
    constructor(props) {
        super(props);
        this.handleLike = this.handleLike.bind(this);
    }

    handleLike(event) {
        event.preventDefault();
        this.props.dispatch(toggleLike(this.props.postId));
    }

    render() {
        return (
            <div className="ui extra content grid centered">
                <div className="ui labeled button" onClick={this.handleLike}>
                    <div className="ui violet button">
                        <i className="diamond icon"/> {this.props.text}
                    </div>
                    <a className="ui basic violet left pointing label">
                        {this.props.likes}
                    </a>
                </div>
            </div>
        )
    }
}


LikeButton.propTypes = {
    likes: React.PropTypes.number,
    text: React.PropTypes.string
};

export default connect()(LikeButton);