import React from 'react';

import AvatarHeader from  './AvatarHeader';
import LikeButton from './LikeButton';

const Post = (props) => {
        return (
            <div className="ui card fluid animated fadeInUp">
                <AvatarHeader  username={props.username} avatarPath={props.avatarPath}/>
                <div className="image">
                    <img src={props.imagePath}/>
                </div>
                {(() => {
                    if(props.description != "") {
                        return (
                            <div className="content">
                                <div className="description">
                                    {props.description}
                                </div>
                            </div>
                        )
                    }
                })()}
                {JSON.parse(localStorage.getItem('user'))  ? <LikeButton likes={props.likes} postId={props.postId} text={props.text} /> : null }
            </div>
        )
};

Post.defaultProps = {
    description: "",
    imagePath:  "http://www.chilirhea.net/wp-content/uploads/2014/10/placeholder-wide-image3.png"
};
Post.propTypes = {
    description: React.PropTypes.string,
    imagePath: React.PropTypes.string,
};


export default Post;