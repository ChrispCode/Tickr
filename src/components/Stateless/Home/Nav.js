import React from 'react';
import {Link} from 'react-router';

const Nav = (props) => {
    return (
        <div className="ui small fixed top menu transition">
            <div className="ui container">
                <a className="item"><b>{props.projectName}</b></a>
                {(() => {
                    let authCredentials = JSON.parse(localStorage.getItem('user'));
                    if (authCredentials) {
                        return (
                            <div className="right menu">
                                <div className="ui floating dropdown icon item blue">
                                    {authCredentials.username}<i className="dropdown icon"></i>
                                    <div className="menu">
                                        <a className="item" onClick={props.handleLogout}>Log out</a>
                                    </div>
                                </div>
                            </div>
                        )
                    } else {
                        return (
                            <div className="right menu">
                                <Link className="item" to="/login">
                                    <button className="ui primary button">Log in</button>
                                </Link>
                                <Link className="item" to="/register">
                                    <button className="ui button">Register</button>
                                </Link>
                            </div>
                        )
                    }
                })()}
            </div>
        </div>
    )
};

Nav.defaultProps = {
    projectName: "Project",
};
Nav.propTypes = {
    projectName: React.PropTypes.string,
};

export default Nav;
