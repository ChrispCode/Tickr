import React from 'react';

const Form = (props) => {
    return (
        <form className="ui large form" onSubmit={props.onSubmit}>
            <div className="ui stacked segment">
                {props.children}
                <button type="submit" className="ui fluid large primary button">{props.buttonText}</button>
            </div>
            {props.hasError ?  <div className="ui negative message"><p>{props.errorMessage}</p></div> : null}
        </form>
    )
};

Form.defaultProps = {
    buttonText: "Enter",
};
Form.propTypes = {
    buttonText: React.PropTypes.string,
};

export default Form;
