import React from 'react';
import {Link}  from 'react-router';

const LinkBox = (props) => {
    return (
        <div className="ui message">
            {props.beforeText}<Link to={props.link}>{props.insideText}</Link>
        </div>
    )
};

LinkBox.defaultProps = {
    beforeText: "",
    insideText: "Enter text here",
    link: "/"
};
LinkBox.propTypes = {
    beforeText: React.PropTypes.string,
    insideText: React.PropTypes.string,
    link: React.PropTypes.string,
};

export default LinkBox;
