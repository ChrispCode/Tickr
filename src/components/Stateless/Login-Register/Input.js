import React from 'react';

const Input = (props) => {
    return (
        <div className="field">
            <input type={props.type} name={props.name} placeholder={props.placeholder} value={props.value}
                   onChange={props.onChange}/>
        </div>
    )
};

Input.defaultProps = {
    placeholder: "",
    type: "text"
};
Input.propTypes = {
    name: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    type: React.PropTypes.string
};
export default Input;
