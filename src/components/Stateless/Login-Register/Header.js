import React from 'react';
import reactCSS from 'reactcss';
import {Link} from 'react-router';

const styles = reactCSS({
    'default': {
        TextSize: {
            fontSize: '3em',
        },
    },
});

const Header = (props) => {
    return (
        <h1 className="ui header" style={styles.TextSize}>
            <Link to="/" className="content">{props.text}</Link>
        </h1>
    )
};

Header.defaultProps = {
    text: "",
};
Header.propTypes = {
    text: React.PropTypes.string,
};

export default Header;
