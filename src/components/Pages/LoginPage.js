import React from 'react';
import $ from 'jquery';
import { browserHistory } from 'react-router'


import  Header from '../Stateless/Login-Register/Header';
import  Input from '../Stateless/Login-Register/Input';
import  Form from '../Stateless/Login-Register/Form';
import  LinkBox from '../Stateless/Login-Register/LinkBox';


class LoginPage extends React.Component {
    constructor() {
        super();
        this.state = {username: "", password: "", errorMessage: "", hasError: false};
        this.gridStyle = { height: '100%'};
        this.columnStyle = {maxWidth: '300px', marginTop: '200px', marginBottom: '200px'};
        this.handleChange =  this.handleChange.bind(this);
    }

    componentWillMount() {
        if(localStorage.getItem('user')) {
            browserHistory.push('/');
        }
    }

    handleChange(event) {
        let state = this.state;
        state[event.target.name] = event.target.value;
        this.setState(state);
    }

    handleSubmit(event) {
        event.preventDefault();
        var self = this;

        let data = {
            username: this.state.username,
            password: this.state.password
        };

        $.ajax({
            type: 'POST',
            url: '/api/login',
            data: data
        })
        .done((data) =>  {
            localStorage.setItem('user', JSON.stringify(data));
            browserHistory.push('/');
        })
        .fail((boomErr) =>{
            self.setState({hasError: true, errorMessage: JSON.parse(boomErr.responseText).message});
        });
    }

    render() {
        return (
            <div className="ui middle aligned center aligned grid" style={this.gridStyle}>
                <div className="column" style={this.columnStyle}>
                    <Header text ="Tickr"/>
                    <Form buttonText="Login" onSubmit={this.handleSubmit.bind(this)} hasError={this.state.hasError} errorMessage={this.state.errorMessage}>
                        <Input name="username" placeholder="Username" value={this.state.username} onChange={this.handleChange}  />
                        <Input name="password" placeholder="Password" value={this.state.password} onChange={this.handleChange} type="password"  />
                    </Form>
                    <LinkBox beforeText="New to us? " link="/register" insideText="Register"/>
                </div>
            </div>
        )
    }
}

export default LoginPage;