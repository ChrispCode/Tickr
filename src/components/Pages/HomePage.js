import React from 'react';
import {connect} from 'react-redux';
import $ from 'jquery';
import {browserHistory} from 'react-router';
import reactCSS from 'reactcss';

import Nav from  '../Stateless/Home/Nav';
import Post from  '../Stateless/Home/Post';
import CreatePostForm  from  '../Stateful/CreatePostForm'

class HomePage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.handleLogout = this.handleLogout.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.styles = reactCSS({
            'default': {
                ColumnStyle: {
                    marginTop: '20px'
                },
            },
        });
    }

    handleLogout(event) {
        event.preventDefault();
        localStorage.removeItem('user');
        $.ajax({
            url: '/api/logout',
        }).done(() => {
            browserHistory.push('/');
        });
    }


    componentDidMount() {
        $('.ui.dropdown').dropdown();
        $('.createPost .image').dimmer({
            on: 'hover'
        });

        // var self = this;
        // var pusher = new Pusher('8a903e0a2e71cd48b716', {
        //     cluster: 'eu',
        //     encrypted: true
        // });
        //
        // var channel = pusher.subscribe('tickr-home');
        // channel.bind('newPost', (data) =>  {
        //     var post = [data].concat(self.state.posts);
        //     self.setState({posts: post });
        // });
    }

    render() {
        let posts = this.props.posts.map(post => {
            if (localStorage.getItem('user')) {
                return (
                    <Post likes={post.likes.length}
                          text={post.likes.indexOf(JSON.parse(localStorage.getItem('user')).id) > -1 ? "Unlike" : "Like"}
                          postId={post.id} key={post.id} username={post.user.username}
                          avatarPath="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"
                          imagePath={post.imagePath} description={post.description} />
                )
            } else {
                return (
                    <Post postId={post.id} key={post.id} username={post.user.username}
                          avatarPath="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"
                          imagePath={post.imagePath} description={post.description}/>
                )
            }
        });
        return (
            <div>
                <Nav projectName="Tickr" handleLogout={this.handleLogout}/>
                <div className="ui container holder grid stackable doubling" style={this.styles.ColumnStyle}>
                    <div className="eight wide column centered" id="posts"> {/*centered*/}
                        {localStorage.getItem('user') ?
                            <CreatePostForm username={JSON.parse(localStorage.getItem('user')).username} /> : null}
                        {posts}
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        posts: state.posts
    };
}


export default connect(mapStateToProps)(HomePage);
