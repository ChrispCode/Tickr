import React from 'react';
import $ from 'jquery';
import {browserHistory} from  'react-router';

import  Header from '../Stateless/Login-Register/Header';
import  Input from '../Stateless/Login-Register/Input';
import  Form from '../Stateless/Login-Register/Form';
import  LinkBox from '../Stateless/Login-Register/LinkBox';


class RegisterPage extends React.Component {
    constructor() {
        super();
        this.gridStyle = { height: '100%'};
        this.columnStyle = {maxWidth: '300px', marginTop: '200px', marginBottom: '200px'};
        this.state = {username: "", email: "", password: ""};
        this.handleChange =  this.handleChange.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        var self = this;

        let data = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password
        };

        $.ajax({
            type: 'POST',
            url: '/api/register',
            data: data
        })
        .done((data) =>  {
            localStorage.setItem('user', JSON.stringify(data));
            browserHistory.push('/');
        })
        .fail((boomErr) =>{
            self.setState({hasError: true, errorMessage: JSON.parse(boomErr.responseText).message});
        });
    }

    handleChange(event) {
        let state = this.state;
        state[event.target.name] = event.target.value;
        this.setState(state);
    }

    render() {
        return (
            <div className="ui middle aligned center aligned grid" style={this.gridStyle}>
                <div className="column" style={this.columnStyle}>
                    <Header text ="Tickr"/>
                    <Form buttonText="Register" onSubmit={this.handleSubmit.bind(this)} hasError={this.state.hasError} errorMessage={this.state.errorMessage}>
                        <Input name="username" placeholder="Username" value={this.state.username} onChange={this.handleChange}/>
                        <Input name="email" placeholder="Email" type="email" value={this.state.email} onChange={this.handleChange}/>
                        <Input name="password" placeholder="Password" type="password" value={this.state.password} onChange={this.handleChange}/>
                    </Form>
                    <LinkBox beforeText="Already registered? " link="/login" insideText="Login"/>
                </div>
            </div>
        )
    }
}

export default RegisterPage;
