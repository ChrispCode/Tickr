import React from 'react';
import {connect} from 'react-redux';

class TestPage extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <h1>Test Page</h1>
                {this.props.posts.map((object) => {
                    return <p>{object.imagePath}</p>
                })}
            </div>
        );
    }

}
function mapStateToProps(state, ownProps) {
    return {
        posts: state.posts
    }
}

export default connect(mapStateToProps)(TestPage)