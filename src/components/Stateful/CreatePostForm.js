import React from 'react';
import {connect} from 'react-redux';
import FormData from 'form-data';
import {addPost} from '../../actions/postActions';

class CreatePostForum extends React.Component {

    constructor(props) {
        super(props);
        this.state = {description: "", postImage: "http://ciyuanhawker.com.sg/wp-content/uploads/2015/01/img-placeholder.jpg", imageName:"Choose a file"};
        this.contentStyle = {marginBottom: '-5px'};
        this.imageStyle = {marginTop: '-5px'};
        this.submit = this.submit.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleFileChange =  this.handleFileChange.bind(this);
    }

    handleTextChange(event) {
        let state = this.state;
        state[event.target.name] = event.target.value;
        this.setState(state);
    }

    handleFileChange(event) {
        console.log('Selected file:', event.target.files[0]);
        var self = this;
        this.setState({imageName: event.target.files[0]["name"]});
        var reader = new FileReader();
        reader.onload = function (event) {
            self.setState({postImage: event.target.result});
        };
        reader.readAsDataURL(event.target.files[0]);
    }

    submit(event) {
        event.preventDefault();
        var fd = new FormData();
        var file = document.getElementById('file').files[0];
        fd.append('file', file);
        fd.append('description', this.state.description);
        this.props.dispatch(addPost(fd));
        this.setState({description: "", postImage: "http://ciyuanhawker.com.sg/wp-content/uploads/2015/01/img-placeholder.jpg", imageName:"Choose a file"});
    }

    render() {
        return (
        <div className="ui card fluid createPost">
            <div className="content" style={this.contentStyle}>
                <img className="ui avatar image"
                     src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"
                     style={this.imageStyle}/> {this.props.username}
            </div>
            <div className="blurring dimmable image">
                <div className="ui dimmer">
                    <div className="content">
                        <div className="center">
                            <input type="file" style={{width: '0.1px', height: '0.1px', opacity: '0', overflow: 'hidden', position: 'absolute', zIndex: '-1'}} accept='image/*'  placeholder="Add image" className="" onChange={this.handleFileChange} id="file"/>
                            <label htmlFor="file" className="ui inverted button">{this.state.imageName}</label>
                            <div className="">
                            </div>
                        </div>
                    </div>
                </div>
                <img src={this.state.postImage}/>
            </div>
            <div className="content">
                <div className="description">
                    {this.state.description}
                </div>
            </div>
            <div className="extra content" >
                <div className="ui transparent large left icon input fluid">
                    <i className="comment outline icon" />
                    <input name="description"  type="text" placeholder="Add Description..." value={this.state.description} onChange={this.handleTextChange}/>
                    <button className="ui button teal small" style={{marginRight: '-5px'}}  onClick={this.submit}>Post</button>
                </div>
            </div>
        </div>
        )
    }
}

export default connect()(CreatePostForum);