import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import {IndexRoute, Router, Route, browserHistory } from 'react-router';
import {Provider} from 'react-redux';

import configureStore from '../store/configureStore';
import {loadPosts} from '../actions/postActions';

import HomePage from './Pages/HomePage';
import LoginPage from './Pages/LoginPage';
import Register from './Pages/RegisterPage';
import TestPage from './Pages/TestPage';

const store = configureStore();
store.dispatch(loadPosts());

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Router path="/">
                <IndexRoute name="home" component={HomePage}/>
                <Route name="login" path="/login" component={LoginPage} />
                <Route name="register" path="/register" component={Register}/>
                <Route name="test" path="/test" component={TestPage}/>
            </Router>
        </Router>
    </Provider>
, document.getElementById('app'));