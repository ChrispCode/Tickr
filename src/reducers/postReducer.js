
export default function postReducer(state = [], action) {
    switch(action.type) {
        case 'LOAD_POSTS_SUCCESS':
            return action.posts;
        case 'ADD_POST_SUCCESS':
            return [action.post, ...state];
        case 'TOGGLE_LIKE_SUCCESS':
            return state.map(post => {
                console.log(...post.likes);
               if(post.id !== action.data.postId) {
                   return post;
               } else {
                   function deleteItem(items, del) {
                       return items.filter(item => item !== del);
                   }
                   if(action.data.liked > -1) {
                       return Object.assign({}, post, {likes: deleteItem(post.likes, action.data.userId)});
                   } else {
                       return Object.assign({}, post, {likes: [...post.likes, action.data.userId]})
                   }
               }
            });
        default:
            return state;
    }
}
