import axios from 'axios';

export function loadPostsSuccess(posts) {
    return {type: 'LOAD_POSTS_SUCCESS', posts};
}

export function loadPosts() {
    return function (dispatch) {
        return axios.get('/api/getPosts').then((response) => {
            console.log(response.data);
            dispatch(loadPostsSuccess(response.data));
        })
        .catch(function (error) {
            throw(error);
        });
    };
}

export function addPostSuccess(post) {
    return {type: 'ADD_POST_SUCCESS', post};
}

export function addPost(fd) {
    return function (dispatch) {
        axios({
            method: 'post',
            url: '/api/addPost',
            data: fd
        }).then((response) =>  {
            dispatch(addPostSuccess(response.data));
        }).catch((boomErr) =>{
            console.log(boomErr);
        });
    };
}

export function toggleLikeSuccess(data) {
    return {type: 'TOGGLE_LIKE_SUCCESS', data};
}

export function toggleLike(postId) {
    return function (dispatch) {
        axios({
            method: 'post',
            url: '/api/like',
            data: { postId: postId}
        }).then((response) =>  {
            dispatch(toggleLikeSuccess(response.data));
        }).catch((boomErr) =>{
            console.log(boomErr);
        });
    };
}
