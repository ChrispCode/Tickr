//Thinky ORM setup and export
const config = require('../config');

let thinkyConfig = {
    host: config.host,
    port: config.port,
    db: config.db
};

module.exports = require('thinky')(thinkyConfig);