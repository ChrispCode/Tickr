var thinky = require('../thinky');
var type = thinky.type;

//Post model for RethinkDB
let Post = thinky.createModel("posts", {
    id: type.string(),
    userId: type.string(),
    description: type.string(),
    imagePath: type.string(),
    createdOn: type.date(),
    likes: type.array()
});

module.exports = Post;

//Database relations for the model
let User = require('./user');
Post.belongsTo(User, 'user', 'userId', 'id');