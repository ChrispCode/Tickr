var thinky = require('../thinky');
var type = thinky.type;

//User model for RethinkDB
let User = thinky.createModel("users", {
    id: type.string(),
    username: type.string(),
    email: type.string().email(),
    password:type.string()
});

module.exports = User;

//Database relations for the model
let Post = require('./post');
User.hasMany(Post, 'posts', 'id', 'userId');
