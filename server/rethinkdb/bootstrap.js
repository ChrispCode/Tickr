//Bootstrap file for creating the database as well as all tables in the project
const r = require('rethinkdb');
const async = require('async');
const config = require('../config');

const createDb = (next) => {
    r.connect((err, conn) => {
        r.dbCreate(config.db).run(conn, (err, res) => {
            conn.close();
            next(err, res);
        })
    });
};

const createTable = (name, next) => {
    r.connect({db: config.db}, (err, conn) => {
        r.tableCreate(name).run(conn, (err, res) => {
            conn.close();
            next(err, res);
        })
    });
};

const createTables = (next) => {
    async.map(["users", "posts"], createTable, next);
};

async.series({
    created: createDb,
    tables: createTables,
}, (err, res) => {
    console.log(res);
});