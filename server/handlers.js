//Importing rethinkDB and Thinky ORM
const thinky = require('./rethinkdb/thinky');
const r = require('./rethinkdb/thinky').r;

//Getting the necessary modules
const User = require('./rethinkdb/models/all').User;
const Post = require('./rethinkdb/models/all').Post;

//Importing bcrypt for hash user password store
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);

//Importing and invoking pusher - real-time
// const Pusher = require('pusher');
// const pusher = new Pusher({
//     appId: '230824',
//     key: '8a903e0a2e71cd48b716',
//     secret: 'cee18e6c7bb33640d301',
//     cluster: 'eu',
//     encrypted: true
// });

//Importing fs for saving the files
const fs = require('fs');

//Importing Hapi Boom for server error responses
const Boom = require('boom');

class Handlers {

    static login(request, reply) {
        //Checking for entered user credentials
        r.table('users').filter({username: request.payload.username}).run().then((users) => {
            //If nothing is found -> return error boom
            if(users.length < 1) {
                return reply(Boom.unauthorized('You haven\' registered yet! Please do so!'));
            }
            //Else log user in with cookie-auth-cookie
            else if(bcrypt.compareSync(request.payload.password, users[0].password)) {
                let uuid = 1;
                let account = {
                    id: users[0].id,
                    username: users[0].username,
                    email: users[0].email
                };
                const sid = String(++uuid);
                //Set the authentication cookie
                request.server.app.cache.set(sid, { account: users[0]}, 0, (err) => {
                    if (err) {
                        reply(err);
                    }
                    request.cookieAuth.set({ sid: sid });
                    return reply(account);
                });
            }
            else {
                return reply(Boom.unauthorized('Please check your username and password and try again!'));
            }
        });
    }

    static register(request, reply) {
        //Checking for existing username with entered user credentials
        r.table('users').filter({username: request.payload.username}).run().then((user) => {
            //If not -> create the user
            if(user.length < 1) {
                var hashPassword = bcrypt.hashSync(request.payload.password, salt);
               let newUser = new User({
                   username: request.payload.username,
                   password: hashPassword,
                   email: request.payload.email,
               });
                newUser.saveAll().then((result) => {
                    let uuid = 1;
                    let account = {
                        id: result.id,
                        username: result.username,
                        email: result.email
                    };
                    const sid = String(++uuid);
                    //Authenticate user
                    request.server.app.cache.set(sid, { account: result}, 0, (err) => {
                        if (err) {
                            reply(err);
                        }
                        request.cookieAuth.set({ sid: sid });
                        return reply(account);
                    });
                });
            }
            //Else return error book -> the email has been user before
            else {
                return reply(Boom.unauthorized('This username has already been taken'));
            }
        });
    }

    static logout(request, reply) {
        //Delete authentication cookie
        request.cookieAuth.clear();
        return reply(true);
    };

    static addPost(request, reply) {
        //Generate new image link as well as fileStream
        let filePath = `public/images/${request.auth.credentials.username}-${Date.now()}-${request.payload.file.hapi.filename}`;
        let file = fs.createWriteStream(filePath);
        //Load the image file to the newly created stream
        request.payload.file.on('data', function (data) {
           file.write(data);
        });
        //when loading finishes create a new post model document
        request.payload.file.on('end', function () {
            let post = new Post({
                createdOn: Date.now(),
                description: request.payload.description,
                imagePath: filePath,
                userId: request.auth.credentials.id,
                likes: []
            });
            //Save the document
            post.saveAll({user: true}).then((post) => {
                User.get(post.userId).then((user) => {
                    post.user = user;
                    //Trigger new pusher real-time event
                    console.log(post);
                    reply(post);
                });
            });

        });
    }

    static getPosts(request, reply) {
        //Get post from the database and reply them
        let skips = request.params.skips ? request.params.skips : 0;
        Post.getJoin({user: true}).orderBy(r.desc('createdOn')).skip(skips).run().then((result) => {
            reply(result);
        });
    }

    static like(request, reply) {
        //Get the desired post
        Post.get(request.payload.postId).run().then((post) => {
            //Add a like from the authenticated user -> Add his id to the likes array of the model
            let liked = post.likes.indexOf(request.auth.credentials.id);
            function deleteItem(items, del) {
                return items.filter(item => item !== del)
            }
            //Like or Unlike depending on the case
            if(liked > -1) {
                post.likes = deleteItem(post.likes, request.auth.credentials.id);
            } else {
                post.likes = post.likes.concat(request.auth.credentials.id);
            }
            post.saveAll();
            //Return positive reply
            reply({liked, postId: post.id, userId: request.auth.credentials.id});
        });
    }
}

module.exports = Handlers;
