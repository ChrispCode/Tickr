//Configuration variables throughout the project
exports.host = 'localhost';
exports.port = 28015;
exports.db = 'tickr';
exports.hapiPort = 9000;
exports.hapiGoodInterval = 100000;