require('babel-core/register')({presets: ['es2015', 'react']});
const Hapi = require('hapi');
const config = require('./config');
const CookieAuth = require('hapi-auth-cookie');

//Creating the server
const server = new Hapi.Server();
server.connection({port: 9000});

const options = {
    ops: {
        interval: config.hapiGoodInterval
    },
    reporters: {
        console: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ log: '*', response: '*' }]
        }, {
            module: 'good-console'
        }, 'stdout'],
        file: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ ops: '*' }]
        }, {
            module: 'good-squeeze',
            name: 'SafeJson'
        }, {
            module: 'good-file',
            args: ['./logs/fixtures/file_log']
        }]
    }
};
server.register(CookieAuth, (err) => {
    if (err) {
        throw err;
    }
});

//Registering good logging
server.register({
    register: require('good'),
    options,
}, (err) => {
    if (err) {
        return console.error(err);
    }
    server.start(() => {
        console.info(`Server started at ${ server.info.uri }`);
    });
});

const cache = server.cache({ segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000 });
server.app.cache = cache;

//Creating the authentication strategy
server.auth.strategy('session', 'cookie', true, {
    password: 'Vaj57zED9nsYeMJGP2hnfaxU874t6DV5',
    cookie: 'sid',
    redirectTo: '/login',
    isSecure: false,
    validateFunc: function (request, session, callback) {

        cache.get(session.sid, (err, cached) => {

            if (err) {
                return callback(err, false);
            }

            if (!cached) {
                return callback(null, false);
            }

            return callback(null, true, cached.account);
        });
    }
});

server.route(require('./routes'));

