var Handlers = require('./handlers');
// var Joi = require('joi');
var Routes = [
    {
        path: '/api/like',
        method: 'POST',
        config: {
            handler: Handlers.like,
        }
    },
    {
        path: '/api/getPosts/{skips?}',
        method: 'GET',
        config: {
            handler: Handlers.getPosts,
            auth: false
        }
    },
    {
        path: '/api/addPost',
        method: 'POST',
        config: {
            handler: Handlers.addPost,
            payload: {
                maxBytes: 104857600,
                output: 'stream',
                parse: true,
                allow: 'multipart/form-data'
            }
        }
    },
    {
        path: '/api/register',
        method: 'POST',
        config: {
            handler:  Handlers.register,
            auth: { mode: 'try' },
            plugins: {
                'hapi-auth-cookie': { redirectTo: false }
            }
        }

    },
    {
        method: ['POST'],
        path: '/api/login',
        config: {
            handler: Handlers.login,
            auth: { mode: 'try' },
            plugins: {
              'hapi-auth-cookie': { redirectTo: false }
            }
        }
    },
    {
        path: '/api/logout',
        method: 'GET',
        handler: Handlers.logout
    }

];

module.exports = Routes;
