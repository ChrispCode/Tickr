//External JS
window.$ = window.jQuery = require("jquery");
require('./bower_components/semantic/dist/semantic');

//Internal Javascript
require('./src/components/App');

//External CSS
require('./bower_components/semantic/dist/semantic.min.css');
require('./bower_components/animate.css/animate.min.css');


