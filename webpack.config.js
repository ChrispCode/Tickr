var webpack = require("webpack");

module.exports = {
    entry: ["./index.js"],
    output: {
        filename: "bundle.js",
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                }
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|jpeg|svg|woff|woff2|ttf|eot)$/i,
                loaders: ['file-loader']
            }

        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $:      "jquery",
            jQuery: "jquery"
        })
    ],
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    devServer: {
        // Enable history API fallback so HTML5 History API based
        // routing works. This is a good default that will come
        // in handy in more complicated setups.
        historyApiFallback: true,

        // Display only errors to reduce the amount of output.
        // stats: 'errors-only',
        stats: 'normal',

        host: process.env.HOST || 'localhost',
        port: process.env.PORT || 3000,

        proxy: {
            '/api/*': {
                target: 'http://localhost:9000/',
                secure: false
            }
        },
    },
};